# A raspberry 3 OS

Support:
1. UART
2. Framebuffer
3. update over serial
4. Qemu

## UART
Using GPIO 14 and 15

## update over serial

You can update a kernel that is connected a given TTY with
`TTY=/dev/MYTTY make update_serial`

This is mainly usefull for kernel running on a real board but to use it with QEMU, you have to modify the serial opt to be
`-serial pty`

and then
`TTY=/TTY_VERBOSED_BY_QEMU make update_serial`

## QEMU

You can test it with
` make run`

### Qemu with a debugger

`make debug`

# Install on sdcard

Create a FAT32 partion and copy kernel and kernel8.img
