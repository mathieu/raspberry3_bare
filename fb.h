#pragma once

struct fbst {
    unsigned char *fbp;
    unsigned int width;
    unsigned int height;
    unsigned int pitch;
    unsigned int isrgb;
};

unsigned char *fb_init(int width, int height);
struct fbst * fb_get();
void fb_print(int x, int y, char *s);
