#pragma once

void printhex(unsigned int d);
int strlen(char s[]);
void reverse(char s[]);
void itoa(unsigned int n, char s[]);
void printdec(unsigned int d);
void printclock();
int strcmp(const char s1[], const char s2[]);
int readline(char *buf, int maxlen);

/* From asm_utils.S*/
int get_el(void);
