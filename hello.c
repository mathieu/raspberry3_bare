#include "fb.h"
#include "mbox.h"
#include "static_tools.h"
#include "uart.h"
#include "utils.h"

#define FB_WIDTH 640
#define FB_HEIGHT 480
#define BUFF_SIZE 100

extern unsigned long int __ld_kernel_begin;
int kernelmain(void)
{
  struct fbst *fb;
  unsigned char *ptr;

  uart_init();
  puts("starting kernel\n");
  char buffer[BUFF_SIZE];
  readline(buffer, BUFF_SIZE);

  if (strcmp(buffer, "kernel") == 0) {
    if(copy_kernel()){
      puts("Checksum error");
    }
  }
  if (fb_init(FB_WIDTH, FB_HEIGHT) == 0) {
    puts("Fail to init framebuffer");
  }
  mbox[0] = 8 * 4;
  mbox[1] = MBOX_REQUEST;
  mbox[2] = MBOX_TAG_GETVCOREMEM;
  mbox[3] = 8;
  mbox[4] = 0;
  mbox[5] = 0;
  mbox[6] = 0;
  mbox[7] = MBOX_TAG_LAST;

  if (!mbox_call(MBOX_CH_PROP, mbox)) {
    puts("videocore start at 0x");
    printhex(mbox[5]);
    puts(" - ");
    int mem = mbox[6] / 1024 / 1024;
    printdec(mem);
    puts(" Mo\n");
  } else {
    puts("Error getting videocore mem\n");
  }

  printclock();

  int el = get_el();
  puts("Exception leve: ");
  printdec(el);
  fb  = fb_get();
  ptr = fb->fbp;

  for (unsigned int y = 0; y < fb->height; y++) {
    for (unsigned int x = 0; x < fb->width; x++) {
      if (y % 60 == 0)
        *((unsigned int *)ptr) = 0x00FF0000;
      else if (y % 40 == 0)
        *((unsigned int *)ptr) = 0x0000FF00;
      else if (y % 20 == 0)
        *((unsigned int *)ptr) = 0x000000FF;
      else
        *((unsigned int *)ptr) = 0x00000000;
      ptr += 4;
    }
  }

  fb_print(0, 0, "Hello Dude\n");
  return 0;
}
