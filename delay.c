#include "delay.h"
#include "mmio.h"

#define STC_LOW (*(volatile unsigned *)(IO_BASE + 0x3004))
#define STC_HIGH (*(volatile unsigned *)(IO_BASE + 0x3008))

uint64_t micros() {
  uint32_t high, low;
  high = STC_HIGH;
  low = STC_LOW;
  if (high != STC_HIGH) {
    high = STC_HIGH;
    low = STC_LOW;
  }
  return ((uint64_t)high) << 32 | low;
}
