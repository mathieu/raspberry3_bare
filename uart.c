#include <stdint.h>

#include "uart.h"
#include "gpio.h"


void uart_init(void )
{
	//Active waiting
	while(UART0_FR & (1<<3)){}

	//Stop UART0
	UART0_CR = 0;

	//GPIO 14 and 15 in ALT0
	GPFSEL1 &= ~((7<<12)|(7<<15));
	GPFSEL1 |= (4<<12)|(4<<15);
	GPPUD = 0;
	for(uint8_t i = 0; i < 150; i++){
		asm volatile ("nop");
	}
	GPPUDCLK0 = (1<<14)|(1<<5);
	for(uint8_t i = 0; i < 150; i++){
		asm volatile ("nop");
	}

	//CLR INTR
	GPPUDCLK0 = 0;

	// Setup for 115200bps @3Mhz
	// 3000000 /(115200 * 16) = 1.627
	// 0.627 * 64 + 0.5 = 40.628 -> 40
	UART0_IBRD = 1; //int part
	UART0_FBRD = 40; //fract part
	UART0_LCRH = 0b11 <<5; //format 8n1
	UART0_CR = 0x301; //Active Tx, Rx & FIFO
}

char uart_recv(void)
{
	while (UART0_FR & (1 << 4)) {
	}
	return UART0_DR & 0xFF;
}

void putc(char c){
	//FIFO Full?
	while(UART0_FR & (1<<5)){}

	UART0_DR = c;
}

void puts(const char *s){
	while(*s)
		putc(*s++);
}
