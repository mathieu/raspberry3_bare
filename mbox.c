#include "mbox.h"

// See at https://github.com/raspberrypi/firmware/wiki/Mailbox-property-interface
// For messsage description
// E.g. for Get firmware revision
//
//    Tag: 0x00000001
//    Request:
//        Length: 0
//    Response:
//        Length: 4
//        Value:
//            u32: firmware revision
//  mbox[0] = 4 * 6 // total length in byte
//  mbox[1] = MBOX_REQUEST
//  mbox[2] = 0x00000001 //TAG
//  mbox[3] = max(req_length, resp_length) -> 4
//  mbox[5] = 0 // 0 :request, 0x80000000: response
//  mbox[6] = MBOX_TAG_LAST


// aligned on 16 mutiple so the 4 first bit can be used by the channel
// Should be allocated in the first 2^32bytes
volatile uint32_t __attribute__((aligned(16))) mbox[36];

int mbox_call(uint8_t ch, volatile uint32_t *box)
{
  // mbox is 16 aligned
  uint32_t r = (uint64_t)box | (ch & 0xF);
  while (*MBOX_STATUS & MBOX_FULL) {
    asm volatile("nop");
  }
  *MBOX_WRITE = r;
  while (1) {
    while (*MBOX_STATUS & MBOX_EMPTY) {
      asm volatile("nop");
    }
    if (r == *MBOX_READ)
      /* is it a valid successful response? */
      return box[1] == MBOX_RESPONSE ? 0 : -1;
  }

  return -1;
}
