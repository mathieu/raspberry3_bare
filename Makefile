# Used to generated .d file that deal with header dependencies
CPPFLAGS = -MMD
CROSS=aarch64-linux-gnu-
CC=$(CROSS)gcc
LD=$(CROSS)ld
CFLAGS=-Wall -Wextra -ffreestanding -march=armv8-a+crc -mcpu=cortex-a53
DEBUG_FLAGS += -g -Og -DDEBUG -fno-omit-frame-pointer -fno-inline
LDSCRIPT=rpi3.ld
TTY?=/dev/ttyUSB0

gasmsrc=$(wildcard *.S)
gasmobj=$(gasmsrc:%.S=%.o)
csrc=$(wildcard *.c)
cobj=$(csrc:%.c=%.o)
deps=$(csrc:%.c=%.d) $(gasmsrc:%.S=%.d)

KERNEL=kernel.bin

all:$(KERNEL)

kernel.elf: $(cobj) $(gasmobj) font_psf.o $(LDSCRIPT)
	$(LD) $(LDFLAGS) -o $@ $(gasmobj) $(cobj) font_psf.o -T$(LDSCRIPT) -Map kernel.map

$(KERNEL) kernel.sym &: kernel.elf
	$(CROSS)objcopy -O binary $< $(KERNEL)
	$(CROSS)objcopy --only-keep-debug $< kernel.sym

font_psf.o: font.psf
	$(LD) -r -b binary -o font_psf.o font.psf

clean:
	rm -rf $(cobj) $(gasmobj) $(deps) font_psf.o *.sym *.bin *.elf *.map

run: $(KERNEL)
	qemu-system-aarch64 -machine raspi3b -kernel $< -serial stdio

update_serial:
	$(eval skip := $(shell $(CROSS)objdump -t kernel.elf | grep __ld_kernel_begin | cut -d " " -f 1))
	$(eval skip := $(shell printf "%d" $$((0x$(skip)- 0x80000))))
	./boot_client/boot_send.py -d $(TTY) -b 115200 -k kernel.bin -i -s $(skip)

debug: CFLAGS += $(DEBUG_FLAGS)
debug: CXXFLAGS += $(DEBUG_FLAGS)
debug:$(KERNEL)
	aarch64-linux-gnu-gdb -q -x debug.gdb


ifneq ($(MAKECMDGOALS),clean)
-include $(deps)
endif
