#pragma once

// IO mapping
#define IO_BASE 0x3f000000

// System Timer
#define TIMER_BASE (IO_BASE + 0x3000)

// Intr mapping
#define INTERUPT_BASE (IO_BASE + 0xB000)

// Videocore mbox system
#define VCORE_MBOX (IO_BASE + 0x0000B880)

// Power manager
#define POWER_BASE (IO_BASE + 0x100000)

//Random gen
#define RANDOM_BASE (IO_BASE + 0x104000)

// GPIO mapping
#define GP_BASE (IO_BASE + 0x200000)

// UART0
#define UART0_BASE (IO_BASE + 0x201000)

// Mini-UART mapping or UART1
#define MU_BASE (IO_BASE + 0x215000)

// External Mass Media Controller
#define SD_BASE (IO_BASE + 0x300000)

// USB
#define USB_BASE (IO_BASE + 0x980000)
