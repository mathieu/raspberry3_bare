#include "clock.h"
#include "uart.h"
#include <stdint.h>

// K&R inspirated

void printhex(unsigned int d)
{
	char n;
	for (int c = 28; c >= 0; c -= 4) {
		// get highest tetrad
		n = (d >> c) & 0xF;
		// 0-9 => '0'-'9', 10-15 => 'A'-'F'
		n += n > 9 ? 0x37 : 0x30;
		putc(n);
	}
}

int strlen(char s[])
{
	int i = 0;
	while (s[i] != '\0') {
		++i;
	}
	return i;
}

void reverse(char s[])
{
	int c, i, j;
	for (i = 0, j = strlen(s) - 1; i < j; i++, j--) {
		c    = s[i];
		s[i] = s[j];
		s[j] = c;
	}
}

void itoa(unsigned int n, char s[])
{
	int i = 0;
	do {
		s[i++] = n % 10 + '0';
	} while ((n /= 10) > 0);
	s[i] = '\0';
	reverse(s);
}

void printdec(unsigned int d)
{
	char s[16];
	itoa(d, s);
	for (int i = 0; i < strlen(s); i++) {
		putc(s[i]);
	}
}

void printclock()
{
	static const char *clocktxts[] = {"reserved", "EMMC", "UART",  "ARM",   "CORE",
					  "V3D",      "H264", "ISP",   "SDRAM", "PIXEL",
					  "PWM",      "HEVC", "EMMC2", "M2MC",  "PIXEL_BVB"};

	puts("Clocks:\n");
	for (int i = 1; i <= CLK_PIXEL_BVB; i++) {
		puts(" ");
		puts(clocktxts[i]);
		puts(": ");
		printdec(getclock(i, 0));
		puts("/");
		printdec(getclock(i, 1));
		puts(" Hz\n");
	}
}

int readline(char *buf, int maxlen)
{
	int num = 0;
	while (num < maxlen - 1) {
		char c = uart_recv();
		// It seems like screen sends \r when I press enter
		if (c == '\n' || c == '\0' || c == '\r') {
			break;
		}
		buf[num] = c;
		num++;
	}
	buf[num] = '\0';
	return num;
}

// Returns <0 if s1<s2, 0 if s1==s2, >0 if s1>s2
int strcmp(const char s1[], const char s2[])
{
    int i;
    for (i = 0; s1[i] == s2[i]; i++) {
        if (s1[i] == '\0')
            return 0;
    }
    return s1[i] - s2[i];
}
