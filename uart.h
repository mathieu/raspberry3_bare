#pragma once
#include "mmio.h"

/* PL011 UART */
#define UART0_DR (*(volatile unsigned *)(UART0_BASE + 0x0))
#define UART0_FR (*(volatile unsigned *)(UART0_BASE + 0x18))
#define UART0_IBRD (*(volatile unsigned *)(UART0_BASE + 0x24))
#define UART0_FBRD (*(volatile unsigned *)(UART0_BASE + 0x28))
#define UART0_LCRH (*(volatile unsigned *)(UART0_BASE + 0x2C))
#define UART0_CR (*(volatile unsigned *)(UART0_BASE + 0x30))
#define UART0_IMSC (*(volatile unsigned *)(UART0_BASE + 0x38))
#define UART0_ICR (*(volatile unsigned *)(UART0_BASE + 0x44))

void uart_init(void);
char uart_recv(void);
void putc(char c);
void puts(const char *s);
